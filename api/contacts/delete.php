<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: DELETE');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once '../../config/Database.php';
    include_once '../../models/Contacts.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate contacts object
    $contacts = new Contacts($db);

    // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to update
    $contacts->id = $data->id;

    if($_SERVER['REQUEST_METHOD'] == 'DELETE'){
    // Delete contact
    if($contacts->delete()) {
        echo json_encode(
            array('code' => 200, 'message' => 'Contact Deleted')
        );
    } else {
        echo json_encode(
            array('code' => 500, 'message' => 'Contact Not Deleted')
        );

        }
    }else{
        echo json_encode(["code" => 405, "message" => "Just DELETE method its allow"]);
    }