<?php 
  // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
  
    include_once '../../config/Database.php';
    include_once '../../models/Contacts.php';
  
    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();
    
    // Instantiate blog post object
    $contacts = new Contacts($db);
  
    // Get raw contactsed data
    $data = json_decode(file_get_contents("php://input"));

    if(!empty($data->name) && !empty($data->last_name) && !empty($data->email)){
        $contacts->name = $data->name;
        $contacts->last_name = $data->last_name;
        $contacts->email = $data->email;
        $contacts->phone1 = isset($data->phone1)?$data->phone1:null; //validating the phone1
        $contacts->phone2 = isset($data->phone2)?$data->phone2:null; //validating the phone2

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Create contact
            if($contacts->create()) {
                echo json_encode(
                    array('code' => 200, 'message' => 'Contact Created')
                );
            } else {
                echo json_encode(
                    array('code' => 500,'message' => 'Contact Not Created')
                );
            }
        }else{
            echo json_encode(["code" => 405, "message" => "Just POST method its allow"]);
        }
    }else{
        echo json_encode(
            array('code' => 400, 'message' => 'One or more of the required fields are empty (name, last_name or email)')
        );
    }
    
