<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Contacts.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate contacts object
  $contacts = new Contacts($db);

  // contacts query
  $result = $contacts->read();

  // Get row count
  $num = $result->rowCount();

  // Check if any contactss
  if($_SERVER['REQUEST_METHOD'] == 'GET'){
    if($num > 0) {
      // contacts array
      $contacts_arr = array();
      $contacts_arr["code"] = 200;
      $contacts_arr["data"] = array();
      while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $contacts_item = array(
          'id' => $id,
          'name' => $name,
          'last_name' => $last_name,
          'email' => $email,
          'phone1' => $phone1,
          'phone2' => $phone2
        );
  
        // Push to "data"
        array_push($contacts_arr["data"], $contacts_item);
      }
  
      // Turn to JSON & output
      echo json_encode($contacts_arr);
    } else {
      // No contacts
      echo json_encode(
        array('code' => 404, 'message' => 'No contacts Found')
      );
    }
  }else{
    echo json_encode(["code" => 405, "message" => "Just GET method its allow"]);
  }