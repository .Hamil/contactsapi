<?php 

class Contacts {
    // DB stuff
    private $conn;
    private $table = 'contacts';
    // Post Properties
    public $id;
    public $name;
    public $last_name;
    public $email;
    public $phone1;
    public $phone2;
    // Constructor with DB
    public function __construct($db) {
      $this->conn = $db;
    }
    // Get Posts
    public function read() {
      // Create query
      $query = 'SELECT id, name, last_name, email, phone1, phone2 FROM ' . $this->table;
      
      // Prepare statement
      $stmt = $this->conn->prepare($query);
      // Execute query
      $stmt->execute();
      return $stmt;
    }

    // // Create Post
    public function create() {
          // Create query
          $query = 'INSERT INTO ' . 
            $this->table . ' 
          SET 
            name = :name, 
            last_name = :last_name, 
            email = :email, 
            phone1 = :phone1, 
            phone2 = :phone2';

          // Prepare statement
          $stmt = $this->conn->prepare($query);
          // Clean data
          $this->name = htmlspecialchars(strip_tags($this->name));
          $this->last_name = htmlspecialchars(strip_tags($this->last_name));
          $this->email = htmlspecialchars(strip_tags($this->email));
          $this->phone1 = htmlspecialchars(strip_tags($this->phone1));
          $this->phone2 = htmlspecialchars(strip_tags($this->phone2));

          // Bind data
          $stmt->bindParam(':name', $this->name);
          $stmt->bindParam(':last_name', $this->last_name);
          $stmt->bindParam(':email', $this->email);
          $stmt->bindParam(':phone1', $this->phone1);
          $stmt->bindParam(':phone2', $this->phone2);
          // Execute query
          if($stmt->execute()) {
            return true;
      }
      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);
      return false;
    }
    
    // // Delete Post
    public function delete() {
          // Create query
          $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
          // Prepare statement
          $stmt = $this->conn->prepare($query);
          // Clean data
          $this->id = htmlspecialchars(strip_tags($this->id));
          // Bind data
          $stmt->bindParam(':id', $this->id);
          // Execute query
          if($stmt->execute()) {
            return true;
          }
          // Print error if something goes wrong
          printf("Error: %s.\n", $stmt->error);
          return false;
    }
    
  }

?>