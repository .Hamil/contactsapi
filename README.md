Instrucciones para desplegar el API
    - Poner el proyecto en la carpeta htdocs de XAMPP
    - Crear una base de datos que se llame “contactsapi” (Motor Base de datos es MYSQL)
    - Crear una tabla con la siguiente estructura:
        CREATE TABLE contacts(
            id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
            name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL,
            phone1 VARCHAR(50),
            phone2 VARCHAR(50)
        );
        
Instrucciones para usar el API
    - Ruta para ver todos los contactos (GET)
         http://localhost/contactsapi/api/contacts/read.php
    - Ruta para crear un contacto (POST)
        http://localhost/contactsapi/api/contacts/create.php
            En Postman/Body/Raw debe mandarle un JSON con la siguiente estructura:
                {
	                "name": "Victor",
	                "last_name": "Diaz",
	                "email": "hamil151@hotmail.com",
	                "phone1": "849-403-2354",
	                "phone2": "809-274-8438"
                }
    - Ruta para eliminar un contacto (DELETE)
        http://localhost/contactsapi/api/contacts/delete.php
            En Postman/Body/Raw debe mandarle un JSON con la siguiente estructura:
                {
	                "id": 1
                }
